<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */
get_header(); ?>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=532857810151555&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="content-wrapper">
    <section class="blog-bg">
        <header id="page-header">
        <span id="page-title">HOMME BLOG</span>
        </header>
    </section>
      
        <div class="container content-container"> <!-- inner-container -->
            
            <div class="col-md-8" id="content-column">

                <?php while(have_posts()) : the_post(); ?>

                        <!-- SINGLE POST HEADER -->
                        <div class="entry-header hide-mobile">
                            <div class="entry-details"><?php echo get_the_date('m/d'); ?></div>
                            <div class="entry-title-wrapper">
                                <div class="entry-title"><a href=" <?php the_permalink(); ?> "><?php the_title(); ?></a></div>  
                                <div class="entry-author"><i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?> <i class="fa fa-clock-o"></i><?php the_time('g:i a'); ?></div>                       
                            </div>
                        </div>
                        <div class="mobile-entry-header hide-large">
                            <div class="mobile-entry-title-wrapper">
                                <div class="mobile-entry-title"><?php the_title(); ?></div>  
                                <div class="mobile-entry-author"><i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?><i class="fa fa-clock-o"></i><?php the_time('g:i a'); ?></div>                       
                            </div>
                        </div>
                        <div class="separator-top"></div>    
                        <div class="addthis_native_toolbox" id="post-social-buttons"></div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52f61d3b2284299a"></script>                  
                        <div class="separator"></div>

                        <div class="post-content">
                            
                            <?php  the_content();   ?>                 
                            
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                var disqus_shortname = '10chars';

                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>

                        </div>

                <?php endwhile; wp_reset_query(); ?>
            </div>   <!-- /content-column -->

            <?php get_sidebar(); ?>

        </div> <!-- /inner-container -->


</div> <!-- /page-wrapper -->

<?php get_footer(); ?>
