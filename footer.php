
<footer id="footer">
    <div class="container text-center">
    <div class="col-md-4 footer-col" id="footer-recent-wrapper">
    <h3>OUR BLOG</h3>
    <div class="footer-col-content" id="footer-recent-posts">

        

    <?php
    $args = array( 'numberposts' => '3');
    $recent_posts = wp_get_recent_posts( $args );
    
    foreach( $recent_posts as $recent ){     
    
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($recent["ID"]), 'thumbnail' );
    $url = $thumb['0'];

    echo '<article class="col-md-12 home-recent-wrapper">
    <div class="col-md-4">
        <img class="img-circle img img-responsive" src="'.$url.'" alt="">
    </div>
    <div class="col-md-8">
        <a href="' . get_permalink($recent["ID"]) . '"><p class="home-recent-title">' .   $recent["post_title"].' </p></a>
    </div>    
    </article>';
    }?>
  
    </div>           
    </div>
    <div class="col-md-4 footer-col">
    <h3>SOCIAL</h3>
    <div class="footer-col-content">
        <p>Stay connected with us to get the special deals and the latest information.</p>  
        <div class="footer-share-buttons">
        <i class="fa fa-facebook fa-3x"></i>
        <i class="fa fa-twitter fa-3x"></i>
        <i class="fa fa-google-plus fa-3x"></i>
        </div>  
    </div>    
    </div>
    <div class="col-md-4 footer-col">
        <h3>ACCESS</h3>
        <div class="footer-col-content">
        <img src="<?php echo get_template_directory_uri(); ?>/includes/img/map.png" class="img img-responsive"alt="">    
        <p>Shizuoka, Hamamatsu, Naka-ku</p>
        <p>Shinmei-cho 314-15</p>
        <p>080-3688-3923</p>  
        </div>       
    </div>
    </div>
</footer>

<!-- CREEK SCRIPTS -->
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/scripts.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/includes/js/jquery.swipebox.min.js" type="text/javascript"></script>
 
 <?php if(is_front_page()) {?>
 <script>
  
                $('#down').click(function(){
 
                    $('html, body').animate({
                        scrollTop: $('#services').offset().top
                    }, 1500);
 
                 });

                $('#upload-button').click(function(){
 
                    $('html, body').animate({
                        scrollTop: $('#upload').offset().top
                    }, 1500);
 
                 });

  </script>

  <script type="text/javascript">

        var mainbottom = $('#services').offset().top - 80;

        // on scroll, 
        $(window).on('scroll',function(){

            // we round here to reduce a little workload
            stop = Math.round($(window).scrollTop());
            if (stop > mainbottom) {           
                $('.navbar').css('border-color','#FFF');
            } else {
                $('.navbar').css('border-color','#FCE35C');
            }

        });

    </script>

<?php } ?>
<script type="text/javascript">
    ;( function( $ ) {

         $( '.swipebox' ).swipebox({
            hideBarsOnMobile : false
        });

    } )( jQuery );
</script>
<?php wp_footer(); ?>
</body>
</html>