<?php get_header(); ?>

<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=532857810151555&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="content-wrapper">
    <section class="blog-bg">
        <header id="page-header">
        <span id="page-title"><?php single_post_title(); ?></span>
        </header>
    </section>

        <div class="container content-container"> <!-- inner-container -->
            
            <div class="col-md-8" id="content-column">

                <!-- SETUP PAGINATION -->
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }

                $my_args = array(
                    'post_type' => '',
                    'posts_per_page' => 6,
                    'paged' => $paged);

                $my_query = new WP_Query( $my_args );?>

                <!-- LOOP -->
                <?php   if ( $my_query->have_posts() ) :
                    while ( $my_query->have_posts() ) :
                        $my_query->the_post(); ?>
                    <article class="single-post-container">
                        <!-- STANDARD HEADER ARTICLE -->
                    <div class="entry-header hide-mobile">
                        <div class="entry-details"><?php echo get_the_date('m/d'); ?></div>
                        <div class="entry-title-wrapper">
                            <div class="entry-title"><a href=" <?php the_permalink(); ?> "><?php the_title(); ?></a></div>  
                            <div class="entry-author"><i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?><i class="fa fa-clock-o"></i><?php the_time('g:i a'); ?></div>
                        </div>
                    </div>
                    <div class="mobile-entry-header hide-large">
                            <div class="mobile-entry-title-wrapper">
                                <div class="mobile-entry-title"><a href=" <?php the_permalink(); ?> "><?php the_title(); ?></a></div>  
                                <div class="mobile-entry-author"><i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?><i class="fa fa-clock-o"></i><?php the_time('g:i a'); ?></div>                       
                            </div>
                        </div>
                    <div class="separator"></div>

                        <div class="post-content">

                            <?php the_content();  ?>
              
                      </div>
                     <div class="end-marker"><i class="fa fa-cubes fa-rotate-180"></i></div> 
                    </article>
                    <?php endwhile; wp_reset_postdata();?>

                    <?php if ( function_exists('vb_pagination') ) {
                    vb_pagination( $my_query );} ?>
                <?php else: ?>
                    <h2>Not found</h2>
                <?php endif; ?>

            </div>   <!-- /content-column -->

            <?php get_sidebar(); ?>

        </div> <!-- /inner-container -->

</div> <!-- /page-wrapper -->





<?php get_footer(); ?>
