<?php
/*
Template Name: Homepage
*/
?>
<?php get_header();?>

<!-- PARALLAX 1/MENU -->
<section class="home-bg text-center">

    <div class="logo-text-wrapper">
       <h1 class="logo-text-main">HOMME.</h1> 
       <p class="logo-text-sub">HAMAMATSU</p> 
    </div>
    <i id="down" class="fa fa-chevron-circle-down fa-4x"></i>
           
</section>


<!-- INFORMATION -->
<div id="services">
    
    <div class="container text-center">
        <h1 class="services-title">Our Services</h1>
        <div class="row">
            <div class="container" id="service-content">
                <div class="col-md-4">
                    <a href="http://dev.homme.com/menu#wax"><i class="fa fa-certificate"></i></a>
                    <h4>PERMANENT HAIR REMOVAL</h4>
                    <h4>脱毛</h4>
                </div>
                <div class="col-md-4">
                    <a href="http://dev.homme.com/menu"><i class="fa fa-cube"></i></a>
                    <h4>FACIAL</h4>
                    <h4>フェイシャル</h4>
                </div>
                <div class="col-md-4">
                    <a href="http://dev.homme.com/menu#massage"><i class="fa fa-anchor"></i></a>
                    <h4>MASSAGE</h4>
                    <h4>マッサージ</h4>
                </div>
            </div>
            
            <div class="container" id="introduction-wrapper">
                <h1 class="services-title">Concept</h1>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    オームは、日本で最初にはじめた男性専用エステティックサロン（メンズエステサロン）です。ダイエットやひげ脱毛を考えている方は、体験コースをお試しください。
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>  	 

<?php get_footer(); ?>