<?php get_header();?>

    <div id="content-wrapper">

        <div class="row">

            <div class="container"> <!-- inner-container -->
                <header class="col-md-12" id="page-header">
                    <h1 class="page-title">Error</h1>
                </header>
                <div class="col-md-8" id="content-column">

                <p class="error-message"><?php _e("<!--:en-->Sorry, we couldn't find that page.<!--:--><!--:ja-->ごめんよ！そのページを見つけれなかった！<!--:-->"); ?></p>
                <a href="javascript: history.go(-1)" class="button-flat">BACK</a>

            	</div> <!-- /content-container -->
             <?php get_sidebar(); ?>

        </div> <!-- inner-container -->

    </div> <!-- /page-wrapper -->
    <div>


<?php get_footer(); ?>
