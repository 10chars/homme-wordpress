<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HOMME 浜松<?php wp_title( '|', true, 'RIGHT' ); ?></title>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/js/respond.src.js"></script>
    <![endif]-->
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href='http://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>
</head>

<body>
          
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
            <div class="container">
              <div class="navbar-header"> 
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbarHeadercollapse"> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
                  <span class="icon-bar"></span> 
                </button>          
                <a href="#" class="navbar-brand">
                  <p class="logo">HOMME.</p>
                  <p class="tagline">Health & Beauty</p>
                </a>
                <a id="nav-phone" href="tel:5555555">
                  PH: (053) 555-5555
                </a> 
              </div>
          
              <div class="collapse navbar-collapse navbarHeadercollapse"> 
               <?php    wp_nav_menu( array(
                        'menu' => 'top_menu',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'walker' => new wp_bootstrap_navwalker())
                        );?>                     
              </div> <!-- ENDOFTHEWORLD -->
            </div> 
        </nav>