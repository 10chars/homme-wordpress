<?php get_header();?>

    <div id="content-wrapper">

        <div class="row">

            <div class="container"> <!-- inner-container -->
                <header class="col-md-12" id="page-header">
                    <h1 class="page-title"><?php single_post_title(); ?></h1>
                </header>
                <div class="col-md-8" id="content-column">

                    <?php while(have_posts()) : the_post(); ?>

                        <div class="post-content">
                            <?php the_content(); ?>
                        </div>

                    <?php endwhile; wp_reset_query(); ?>
                </div>   <!-- /content-column -->

                <?php get_sidebar(); ?>

            </div> <!-- /inner-container -->

        </div> <!-- main-row -->

    </div> <!-- /page-wrapper -->


<?php get_footer(); ?>
