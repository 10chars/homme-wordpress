<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */?>

<aside class="col-md-4 sidebar" id="sidebar">
   
    <article class="sidebar-widget" id="facebook-like-widget">
		<h3 class="sidebar-title">LIKE US</h3>
		<div class="sidebar-widget-content">
		<div class="fb-like-box" data-href="http://www.facebook.com/plastician" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
		</div>
	</article>
	
	<!-- Mailing list Widget -->
	 <article class="sidebar-widget" id="mailing-list-widget">
		<h3 class="sidebar-title">JOIN OUR MAILING LIST</h3>
		<div class="sidebar-widget-content">
		<p>Join our mailing list and keep up to date with our latest deals and specials.</p>		

		<form class="form-horizontal" role="form" id="mailing-list-signup-form">
			<div class="form-group">	
			    <div class="col-sm-12">
			      <input type="text" class="form-control" id="name" placeholder="Name">
				</div>
			</div>
			<div class="form-group">
			    <div class="col-sm-12">
			      <input type="email" class="form-control" id="email" placeholder="Email">
			    </div>
			</div>
			  
			<div class="form-group">
			    <div class="form-group">
			      <button type="submit" class="btn btn-success">Sign Up</button>
			    </div>
			</div>
		</form>
		</div> <!-- /sidebar-widget-content -->

	</article>

	
	<!-- Social Widget -->
	<!-- <article class="sidebar-widget" id="social-widget">
		<h3 class="sidebar-title">FOLLOW US</h3>		
		<div class="social-widget-wrapper">
			<a href="http://plus.google.com/u/0/b/105424183852461862212/105424183852461862212/about" rel="author"><i class="fa fa-google-plus fa-3x"></i></a>
			<a href="http://twitter.com/CreekHamamatsu"><i class="fa fa-twitter fa-3x"></i></a>
			<a href="http://www.facebook.com/pages/Casual-Diner-CREEK/172259039646383"><i class="fa fa-facebook fa-3x"></i></a>		
		</div>
	</article> -->

</aside> <!-- end sidebar -->