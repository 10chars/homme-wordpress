<?php
/**
 * Archive Template.
 * 
 */

get_header(); ?>

<div id="content-wrapper">

    <div class="row">

        <div class="container"> <!-- inner-container -->
            <header class="col-md-12" id="page-header">

                <h1 class="page-title">
                    <?php
                    if ( is_category() ) :
                        single_cat_title();

                    elseif ( is_tag() ) :
                        single_tag_title();

                    elseif ( is_author() ) :

                        the_post();
                        printf( __( 'Author: %s', 'lukey' ), '<span class="vcard">' . get_the_author() . '</span>' );

                        rewind_posts();

                    elseif ( is_day() ) :
                        printf( __( 'Day: %s', 'lukey' ), '<span>' . get_the_date() . '</span>' );

                    elseif ( is_month() ) :
                        printf( __( 'Month: %s', 'lukey' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

                    elseif ( is_year() ) :
                        printf( __( 'Year: %s', 'luke' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

                    elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                        _e( 'Asides', 'lukey' );

                    elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                        _e( 'Images', 'lukey');

                    elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                        _e( 'Videos', 'lukey' );

                    elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                        _e( 'Quotes', 'lukey' );

                    elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                        _e( 'Links', 'lukey' );

                    else :
                        _e( 'Archives', 'lukey' );

                    endif;
                    ?>
                              
                </h1>
            </header>

            <div class="col-md-8" id="content-column">
		
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

                <div class="standard-post-header">
                    <a href="<?php the_permalink(); ?>"> <h2 class="sub-title"><?php the_title(); ?></h2> </a>
                    <i class="fa fa-user first"></i><?php the_author(); ?><i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?> <i class="fa fa-clock-o"></i>12:00
                </div>

                <div class="post-content">

                    <?php the_excerpt(); ?>

                </div>
	
			<?php endwhile; ?>

		<?php else : ?>

                <div class="standard-post-header">
                    <h2 class="sub-title"><?php _e( 'Nothing Found', 'lukey' );?></h2>
                </div>
                 <div class="post-content">
                     <?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'lukey' ); ?>
                     <a href=href="javascript: history.go(-1)" class="button-flat">BACK</a>
                </div>

                <?php endif; ?>

                </div> <!--/content column-->
            <?php get_sidebar(); ?>
            </div> <!-- /inner-container -->

        </div> <!-- main-row -->

 </div> <!-- /page-wrapper -->
			

<?php get_footer(); ?>
