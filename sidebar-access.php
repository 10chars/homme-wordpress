<?php
/**
 * User: Lukey
 * Date: 4/19/14
 * 
 */?>

<aside class="col-md-4 sidebar" id="sidebar-access">
   
	<!-- Mailing list Widget -->
	 <article class="sidebar-widget" id="mailing-list-widget">
		<h3 class="sidebar-title">JOIN OUR MAILING LIST</h3>
		<p>Join our mailing list and keep up to date with our latest deals and specials.</p>		

		<form class="form-horizontal" role="form" id="mailing-list-signup-form">
			<div class="form-group">	
			    <div class="col-sm-12">
			      <input type="text" class="form-control" id="name" placeholder="Name">
				</div>
			</div>
			<div class="form-group">
			    <div class="col-sm-12">
			      <input type="email" class="form-control" id="email" placeholder="Email">
			    </div>
			</div>
			  
			<div class="form-group">
			    <div class="form-group">
			      <button type="submit" class="btn btn-success">Sign Up</button>
			    </div>
			</div>
		</form>
	</article>
	
	<!-- Social Widget -->
	<article class="sidebar-widget" id="recent-widget">
		<h3 class="sidebar-title">RECENT POSTS</h3>
		<div class="sidebar-recent-posts">
		    <?php
		    $args = array( 'numberposts' => '3');
		    $recent_posts = wp_get_recent_posts( $args );
		    
		    foreach( $recent_posts as $recent ){     
		    
		    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($recent["ID"]), 'thumbnail' );
		    $url = $thumb['0'];

		    echo '<article class="col-md-12 home-recent-wrapper">
		    <div class="col-md-4">
		        <img class="img-circle img img-responsive" src="'.$url.'" alt="">
		    </div>
		    <div class="col-md-8">
		        <a href="' . get_permalink($recent["ID"]) . '"><p class="home-recent-title">' .   $recent["post_title"].' </p></a>
		    </div>    
		    </article>';
		    }?>
  
    	</div>           

	</article>

</aside> <!-- end sidebar -->