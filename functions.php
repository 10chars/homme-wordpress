<?php

if ( ! function_exists( 'lukey_setup' ) ) :

function lukey_setup() {

    add_editor_style();

    if ( function_exists( 'add_theme_support' ) ) {

		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

        add_theme_support( 'menus' );
        if ( function_exists( 'register_nav_menus' ) ) {
            register_nav_menus(
                array(
                    'main-menu' => 'Primary',
                    'sidebar-menu' => 'Sidebar'
                )
            );

        }

    }

	load_theme_textdomain( 'lukey', get_template_directory() . '/languages' );

}
endif;
add_action( 'after_setup_theme', 'lukey_setup' );


/*HACK TO FIX A FUCK UP WITH REMOVE QTRANSLATE FROM POSTS PAGES*/
add_action('admin_head', 'custom_admin_css');

function custom_admin_css() {
  echo '<style>
    #edit-slug-box {
        margin-top: 15px !important;
    }
  </style>';
}


function init_lukey_widgets() {


    register_sidebar( array(
        'name'          => __( 'Sidebar', 'lukey' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Main sidebar that appears on the right.', 'lukey' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="underline-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Other', 'lukey' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'My sidebar that appears wherever I put it.', 'lukey' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="underline-title">',
        'after_title'   => '</h3>',
    ) );

}
add_action( 'widgets_init', 'init_lukey_widgets' );



/**
 * Enqueue scripts and styles
 */
function lukey_scripts() {

    // Load stylesheets
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri().'/includes/css/bootstrap.min.css' );
    wp_enqueue_style( 'lukey-style', get_stylesheet_uri() );

    // Load jquery & bootstrap.js
    if (!is_admin()) add_action("wp_enqueue_scripts", "my_js_enqueue", 11);
    function my_js_enqueue() {
        wp_deregister_script('jquery');
        wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://code.jquery.com/jquery-1.11.0.min.js", false, null);
        wp_enqueue_script('jquery');
        
        wp_register_script('bootstrap-js', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js", false, null);
        wp_enqueue_script('bootstrap-js');

    }

    // load bootstrap js
    /*wp_enqueue_script('lukey-bootstrapjs', get_template_directory_uri().'/includes/js/bootstrap.min.js', array('jquery') );*/ 
    
}
add_action( 'wp_enqueue_scripts', 'lukey_scripts' );

/**
 * Requires
 */
require get_template_directory() . '/includes/mail-modal.php';
require_once get_template_directory() . '/includes/wp_bootstrap_navwalker.php';

/**
 * Pagination for Bootstrap functions
 * */

function vb_pagination( $query=null ) {

    global $wp_query;
    $query = $query ? $query : $wp_query;
    $big = 999999999;

    $paginate = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'type' => 'array',
            'total' => $query->max_num_pages,
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'prev_text' => __('&laquo;'),
            'next_text' => __('&raquo;'),
        )
    );

    if ($query->max_num_pages > 1) : ?>
        <ul class="pagination">
            <?php
            foreach ( $paginate as $page ) {
                echo '<li>' . $page . '</li>';
            } ?>
        </ul>
    <?php endif; }

/*
 *   MY FILTERS
 * */

function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

function give_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){

    $addClass = 'swipebox';

    if ( preg_match('/<a.*? class=".*?">/', $html) ) {
        $html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $addClass . '$2', $html);

    } else { $html = preg_replace('/(<a.*?)>/', '$1 class="' . $addClass . '" >', $html); }
    return $html;
}

add_filter('image_send_to_editor','give_linked_images_class',10,8);

function add_img_meta($content){
    global $post;
    $rel = $post->ID;
    $addRel = preg_replace('/<a/', '<a rel="'. 'tag' .'"', $content); //TODO: Pointless need to recheck swipebox docs.

    return preg_replace('/<img class="/', '<img class="img-responsive ', $addRel); //TODO: Bit fuckin rough :)
}
add_filter('the_content','add_img_meta');


function bs3_search_form( $form ) {

    $form = '<form action="' . home_url( '/' ). '" method="get" >
            <fieldset>
            <div class="input-group">
                <input type="search" name="s" id="s" placeholder="Search" value="" class="form-control" />
			<span class="input-group-btn">
				<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
			</span>
            </div>
            </fieldset>
            </form>';


    return $form;
}
add_filter( 'get_search_form', 'bs3_search_form' );

add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<a class="more-link" href="' . get_permalink() . '">...もっと見る</a>';
}?>