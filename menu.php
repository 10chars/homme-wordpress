<?php
/*
Template Name: Menu
*/
?>
<?php get_header();?>

<div id="content-wrapper">
<section class="menu-bg">
<header id="page-header">
  <span id="page-title">HOMME <?php single_post_title(); ?></span>
</header>
</section>
   <div class="container content-container">         
	    <div class="col-md-12" id="content-column">
	    	<!-- MENU ITEM -->
	    	<div class="row item">
	    	<a class="anchor" name="facial"></a> 
	    	<div class="col-md-6">
	    		<img src="<?php echo get_template_directory_uri(); ?>/includes/img/facial.jpg" class="img img-responsive" alt="">
	    	</div>
	    	<div class="col-md-6 item-explanation">
	    		<h2>フェイシャル</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.

	    		</p>
	    		
	    		<p class="price">￥1000</p>
	    	</div>
	    	<div class="anchor"><a name="wax"></a></div>
	    	</div>
	    	<!-- /MENU ITEM -->
			
			<!-- MENU ITEM -->
			 
			<div class="row item">

			<div class="col-md-6 item-explanation hide-mobile extra-image">
	    		<h2>ヒゲ脱毛</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    		</p>
	    		<p class="price">￥8000</p>
	    		<img src="<?php echo get_template_directory_uri(); ?>/includes/img/sample.jpg" class="img img-responsive" alt="">

	    	</div>
	    	<div class="col-md-6">
	    		<img src="<?php echo get_template_directory_uri(); ?>/includes/img/beard.jpg" class="img img-responsive" alt="">
	    	</div>
	    	<div class="col-md-6 item-explanation hide-large">
	    		<h2>ヒゲ脱毛</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.

	    		</p>
	    		<p class="price">￥8000</p>
	    	</div>
	    	</div>
	    	<!-- /MENU ITEM -->

	    	<!-- MENU ITEM -->
	    	<div class="row item">  
	    	<div class="col-md-6">
	    		<img src="<?php echo get_template_directory_uri(); ?>/includes/img/male-waxing.jpg" class="img img-responsive" alt="">
	    	</div>
	    	<div class="col-md-6 item-explanation">
	    		<h2>カラダ脱毛</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.

	    		</p>    		
	    		<p class="price">￥80,000</p>
	    	</div>
	    	<a class="anchor" name="massage"></a> 
	    	</div>
	    	<!-- /MENU ITEM -->

	    	<!-- MENU ITEM --> 
			<div class="row item"> 
			<div class="col-md-6 item-explanation hide-mobile">
	    		<h2>アロママッサージ</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.

	    		</p>
	    		<p class="price">￥10,000</p>
	    	</div>
	    	<div class="col-md-6">
	    		<img src="<?php echo get_template_directory_uri(); ?>/includes/img/massage.jpg" class="img img-responsive" alt="">
	    	</div>
	    	<div class="col-md-6 item-explanation hide-large">
	    		<h2>アロママッサージ</h2>
	    		<p>
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.
	    			Lorem ipsum Ut dolor anim sint dolore id id ea eiusmod anim nostrud cillum ut exercitation nisi nostrud qui tempor labore in exercitation Duis qui ut id dolor aute non velit.

	    		</p>
	    		<p class="price">￥10,000</p>
	    	</div>
	    	</div>
	    	<!-- /MENU ITEM -->

	        	
	    </div> <!-- END CONTENT COLUMN --> 
    </div>    
 </div><!-- END CONTENT WRAPPER --> 

<?php get_footer(); ?>    