<?php
/*
Template Name: Access Page
*/
?>

<?php get_header();?>

<div id="content-wrapper">
<section class="access-bg">

<header id="page-header">
<span id="page-title"><?php single_post_title(); ?></span>
</header>

</section>

            <div class="container content-container"> <!-- inner-container -->
                <div class="col-md-8" id="access-content-column">

                    <?php while(have_posts()) : the_post(); ?>
                    
                        <div class="single-post-content">
                            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                            <div id="map" style="width:100%;height:420px;"></div>
                            <script type="text/javascript">
                              var latlng = new google.maps.LatLng(35.658517, 139.701334);
                              var myOptions = {
                                zoom: 17,
                                center: latlng,
                                scrollwheel: false,
                                scaleControl: false,
                                disableDefaultUI: false,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                              };
                              var map = new google.maps.Map(document.getElementById("map"),myOptions);
                              var marker = new google.maps.Marker({
                                map: map, 
                                position: map.getCenter()
                              });      
                              var contentString = '';
                              var infowindow = new google.maps.InfoWindow({
                                content: contentString
                              });
                                          
                              google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map,marker);
                              });
                              
                              infowindow.open(map,marker);
                            </script>             
                        </div>
                        <?php the_content(); ?>
                        <div class="col-md-8" id="contact-form-wrapper">
                        <?php echo do_shortcode('[contact-form-7 id="40" title="Contact form 1"]' ); ?>
                        </div>
                        
                    <?php endwhile; wp_reset_query(); ?>         
                
                </div>   <!-- /content-column -->

                <?php get_sidebar('access'); ?>

            </div> <!-- /inner-container -->

    </div> <!-- /page-wrapper -->





<?php get_footer(); ?>

<?php get_footer(); ?>